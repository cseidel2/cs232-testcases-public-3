// This program depends on the context sensitivity of the addFive method with respect to
// the main method in order to not output that the program prints something that is less
// than zero. 

class DependsOnCSensitivity {
    public static void main(String[] arg){
        Adder a;
        int l;

        a = new Adder();
        l = 0;
        System.out.println(a.addFive(l));
        l = 0 - 100;
        l = a.addFive(l);
        l = l + 100;
        System.out.println(l);
    }
}

class Adder {
	public int addFive(int num) {
		int res;
		res = num + 5;
		return res;
	}
}
