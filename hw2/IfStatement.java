/* This demonstrates how null pointer analysis is flow sensitive. 
Although f = new A(); is called before f.id(); is called, because of the "if" statement,
there is a null pointer error. */

class Main {
    public static void main(String[] a){
        System.out.println(new A().run());          
    }
}

class A {
    A f;
    public int run() {
        int res;

        if (false) {
            f = new A();
        } else {
            res = f.id();
        }

        return res;
    }
    public int id(){
        return 1234;
    }
}
