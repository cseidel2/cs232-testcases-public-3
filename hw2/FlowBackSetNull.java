class Main{
	public static void main(String[] args){
		A a;
		int i;
		a = new A();
		i = a.method(a);
	}
}
class A{
	A field;
	A notSet;
	public int method(A arg) {
    		int i;
    		field = new A();
		i = arg.clearFields();
		return field.action();
	}
	public int clearFields() {
		field = notSet;
		return 0;
	}
	public int action() { return 1; }
}